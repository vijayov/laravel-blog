@extends('layouts/admin')

@section('content')
<table class="table">
    <thead>
        <tr>
            <td>Name</td>
            <td>Email</td>
            <td>Role</td>
            <td>Is Active</td>
            <td>Created At</td>
            <td>Updated At</td>
        </tr>
    </thead>
    <tbody>
     @foreach($users as $user)
        <tr>
        <td>{{$user->name}}</td>
        <td>{{$user->email}}</td>
        <td>{{$user->role->name}}</td>
        <td>{{$user->isActive == 0 ? 'Not Active' : 'Active'}}</td>
        <td>{{$user->created_at->diffForHumans()}}</td>
        <td>{{$user->updated_at->diffForHumans()}}</td>
        </tr>
    @endforeach
    </tbody>
</table>
@endsection