@extends('layouts/admin')

@section('content')
    <h1>Create</h1>
    <div class="col-md-8">
        @include('includes/errors')
         {!!Form::open(['method'=> 'POST' , 'action'=>'AdminUsersController@store','files' => true]) !!}
            <div class="form-group">
                {!! Form::label('name' , 'Name')  !!}
                {!! Form::text('name' ,null, ['class'=>'form-control']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('email' , 'email')  !!}
                {!! Form::email('email' ,null, ['class'=>'form-control']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('role_id' , 'Role')  !!}
                {!! Form::select('role_id' ,$roles,null, ['class'=>'form-control']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('isActive' , 'Status')  !!}
                {!! Form::select('isActive' ,['1' => 'Active' , '0' => 'Not Active'],'0', ['class'=>'form-control']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('image' , 'Profile Photo')  !!}
                {!! Form::file('image') !!}
            </div>
            <div class="form-group">
                {!! Form::label('password' , 'Password')  !!}
                {!! Form::password('password' , ['class'=>'form-control']) !!}
            </div>
            <div class="form-group">
                {!!Form::submit('submit' , ['class' => 'btn btn-primary'])!!}
            </div>
        {!!Form::close() !!}
    </div>
 
@endsection