<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $fillable = [
        'name'
    ];
    protected $primaryKey = 'imageId';

    public function user(){
        return $this->hasMany('App\User');
    }
}
